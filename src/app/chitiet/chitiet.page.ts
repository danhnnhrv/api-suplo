import { Component, OnInit } from '@angular/core';
import { ChitietService } from '../home/services/chitiet/chitiet.service';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-chitiet',
  templateUrl: './chitiet.page.html',
  styleUrls: ['./chitiet.page.scss'],
})
export class ChitietPage implements OnInit {
  product;
  loading;
  handle;
  constructor(
    private route: ActivatedRoute,
    public loadingController: LoadingController,
    public chitiett: ChitietService,
    public router: Router
  ) { }
  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'đang tải..'
    });
    return await this.loading.present();
  }
  async loadData() {
    this.handle = this.route.snapshot.paramMap.get('handle');
    console.log(this.handle);
    await this.presentLoading();
    return await this.chitiett.getProductDetailByHandle(this.handle).then(data => {
      this.product = data;
      this.loadingController.dismiss();
      console.log(this.product);
    })
  }
  ngOnInit() {
    this.loadData();
  }

}

