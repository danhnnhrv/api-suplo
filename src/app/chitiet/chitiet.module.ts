import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ChitietPage } from './chitiet.page';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ChitietService } from '../home/services/chitiet/chitiet.service';
import { HttpService } from '../home/services/http.service';
const routes: Routes = [
  {
    path: '',
    component: ChitietPage
  } 
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HttpClientModule,
    RouterModule.forChild(routes)
  ],
  providers:[
    HttpClient,
    ChitietService,
    HttpService
  ],
  declarations: [ChitietPage]
})
export class ChitietPageModule {}
