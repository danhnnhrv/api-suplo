import { Component, OnInit } from '@angular/core';
import { ProductServiceService } from '../services/product-service/product-service.service';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { $ } from 'protractor';
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})

export class ProductComponent implements OnInit {

  collection;
  sortType;
  colorr: number;
  selectedSortType;
  loading;
  collectionHandle = "all";
  pages: Array<{ number: number}>;
  Number;

  //  collectionHandle = 'onsale'
  constructor(
    public ProductServiceService: ProductServiceService,
    public route: Router,
    public loadingController: LoadingController,
  ) { 
    this.loadData(this.sortType);
  }
  async pageLoading() {
     this.loading = await this.loadingController.create({
      message: 'Đang tải',
      duration: 10000,
      spinner: 'bubbles',
    });
    return await this.loading.present(); 
  }
  
  async loadData(sortType) {
    this.selectedSortType = sortType;
    await this.pageLoading();
    this.ProductServiceService.getCollectionByHandle(this.collectionHandle, 1, this.selectedSortType).then(data => {
      this.collection = data;
      this.loadingController.dismiss();
      
      console.log(this.collection);
      this.pages = [];

      for (let i = 1; i <= this.collection.paginate.pages; i++) {
        this.pages.push({
          number: i,
        });
      }

      if (this.collection.paginate.current_page === 1) {
        document.getElementById("prevPage").style.display= 'none';
      }
      else if (this.collection.paginate.hasNext === 'false') {
        document.getElementById("nextPage").style.display= 'none';
      }
    })
  }
  
  ngOnInit() {
  }
}