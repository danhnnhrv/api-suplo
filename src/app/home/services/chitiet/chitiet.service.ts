import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpService } from '../http.service';

@Injectable({
  providedIn: 'root'
})
export class ChitietService {

  constructor(
    public http: HttpClient,
    private httpService: HttpService
  ) {}

   getProductDetailByHandle(handle){
     return this.http.get(`${this.httpService.apiUrl}/products/${handle}.js`).toPromise();
   }
}
