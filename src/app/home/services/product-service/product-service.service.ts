import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpService } from '../http.service';
@Injectable({
  providedIn: 'root'
})

export class ProductServiceService {

  constructor(
    public http: HttpClient,
    private httpService: HttpService
  ) {}

  getCollectionByHandle(handle,page,sortType){
    return this.http.get(`${this.httpService.apiUrl}/collections/${handle}?view=suplo.json&page=${page}&sort_by=${sortType}`).toPromise();
  }

}
