import { TestBed } from '@angular/core/testing';

import { ColectionProductService } from './colection-product.service';

describe('ColectionProductService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ColectionProductService = TestBed.get(ColectionProductService);
    expect(service).toBeTruthy();
  });
});
