import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpService } from '../http.service';

@Injectable({
  providedIn: 'root'
})
export class ColectionProductService {

  constructor(
    public http: HttpClient,
    private httpService: HttpService
  ) { }
  getcollectionproduct() {
    return this.http.get(`${this.httpService.apiUrl}/collections.json`).toPromise();
  }
}
