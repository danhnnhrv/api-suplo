import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  apiUrl = "https://us-central1-haravan-web-pwa.cloudfunctions.net/haravan/suplo-food";
  token = "";

  getApiUrl() {
    return this.apiUrl;
  }

  constructor(
    public http: HttpClient,
  ) { }

  get(endpoint) {
    let header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Basic ${this.token}`
      })
    }
    return this.http.get(`${this.apiUrl}${endpoint}`, header);
  }

  post(endpoint, payload) {
    return this.http.post(endpoint, payload);
  }
}
